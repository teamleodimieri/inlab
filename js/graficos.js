var ctx = document.getElementById("myChart");
var ctx2 = document.getElementById("myChart2");
var ctx3 = document.getElementById("myChart3");
var myChart = new Chart(ctx, {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'X vs. Y',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }, {
            label: 'R vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }, {
            label: 'X vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }, {
            label: 'Y vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }]
    },
    options: {
        responsive: true,
        scales: {
            yAxes: [{
                type: 'linear',
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                type: 'linear',
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var myChart2 = new Chart(ctx2, {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'V vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }, {
            label: 'Vx vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }, {
            label: 'Vy vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }]
    },
    options: {
        responsive: true,
        scales: {
            yAxes: [{
                type: 'linear',
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                type: 'linear',
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var myChart3 = new Chart(ctx3, {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'A vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }, {
            label: 'Ax vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }, {
            label: 'Ay vs. t',
            data: [],
            fill: false,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        }]
    },
    options: {
        responsive: true,
        scales: {
            yAxes: [{
                type: 'linear',
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                type: 'linear',
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

function updateGraphs(data, t, puntoAnterior, veloAnterior) {
    var dt=1, vx=undefined, vy=undefined, vv=undefined, dtt=1, ax=undefined, ay=undefined, aa=undefined;
    var xx = Number((data.pageX - $('#theCanvas').offset().left).toFixed(0));
    var yy = Number(($('#theCanvas').height() - (data.pageY - $('#theCanvas').offset().top)).toFixed(0));
    var r = Number(Math.sqrt(xx*xx + yy*yy).toFixed(2));
    if (puntoAnterior !== undefined) {
       dt = t - puntoAnterior.t;
       vx = Number(((xx - puntoAnterior.x) / dt).toFixed(2));
       vy = Number(((yy - puntoAnterior.y) / dt).toFixed(2));
       vv = Number(Math.sqrt(vx*vx + vy*vy).toFixed(2));
    }
    if (veloAnterior !== undefined) {
       dtt = puntoAnterior.t - veloAnterior.t;
       ax = Number(((xx - 2*puntoAnterior.x + Number(veloAnterior.x))/dt*dtt).toFixed(2));
       ay = Number(((yy - 2*puntoAnterior.y + Number(veloAnterior.y))/dt*dtt).toFixed(2));
       aa = Number(Math.sqrt(ax*ax + ay*ay).toFixed(2));
    }
    myChart.data.datasets[0].data.push({
        x: xx,
        y: yy
    });
    myChart.data.datasets[1].data.push({
        x: Number(t.toFixed(2)),
        y: r
    });
    myChart.data.datasets[2].data.push({
        x: Number(t.toFixed(2)),
        y: xx
    });
    myChart.data.datasets[3].data.push({
        x: Number(t.toFixed(2)),
        y: yy
    });
    myChart2.data.datasets[0].data.push({
        x: Number(t.toFixed(2)),
        y: vv
    });
    myChart2.data.datasets[1].data.push({
        x: Number(t.toFixed(2)),
        y: vx
    });
    myChart2.data.datasets[2].data.push({
        x: Number(t.toFixed(2)),
        y: vy
    });
    myChart3.data.datasets[0].data.push({
        x: Number(t.toFixed(2)),
        y: aa
    });
    myChart3.data.datasets[1].data.push({
        x: Number(t.toFixed(2)),
        y: ax
    });
    myChart3.data.datasets[2].data.push({
        x: Number(t.toFixed(2)),
        y: ay
    });
    myChart.update();
    myChart2.update();
    myChart3.update();

    return {
        r: r,
        vv: vv,
        vx: vx,
        vy: vy,
        aa: aa,
        ax: ax,
        ay: ay
    };
};